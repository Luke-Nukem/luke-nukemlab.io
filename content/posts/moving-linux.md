+++
title = "Moving Linux to a new partition"
description = ""
date = 2017-03-15
+++

Occasionally I have a need (or just feel a need) to move my Linux installation around it's little universe - it could be for any number of reasons;

* Redoing a partition table
* Backing up an install
* Moving to a new HDD
* Experimenting on an install

Moving a Linux install around is pretty painless compared something like Windows. The basic process is: use rsync to copy /, update /etc/fstab, update the bootloader, have a `choice of beverage`.

## Preparing

The first thing to do is prepare your disks and mount points. Use your partitioning tool of choice to set up a new partition for where you will move the base install (`/`) to - be sure to make it large enough. If you are also moving your `/home`, now is a good time to do it (and if your `/home` isn't a separate partition, you're completely mad!)

Once you've created the partitions on the drive you're going to use, you can mount them with `sudo mkdir /mnt/root && sudo mount /dev/sdXX /mnt/root` and if you're moving `/home`, `sudo mkdir /mnt/root/home && sudo mount /dev/sdXX /mnt/root/home`.

`/dev/sdXX` is the device and partition you'll be using, you can see a list of all current partitions on disks with `lsblk`, eg;

    NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
    sda      8:0    0 119.2G  0 disk
    ├─sda1   8:1    0   156M  0 part /boot/efi
    ├─sda2   8:2    0  25.3G  0 part /
    ├─sda3   8:3    0  25.3G  0 part
    ├─sda4   8:4    0  25.3G  0 part
    ├─sda5   8:5    0  25.3G  0 part
    └─sda6   8:6    0  18.1G  0 part [SWAP]
    sdb      8:16   0 931.5G  0 disk
    └─sdb4   8:20   0 931.5G  0 part /home

Where **sda** is the first disk, and **sdb** is the second, then in the tree, partitions are number *1* to *n*. If I'm moving to the first disks second partition, then I use `sudo mount /dev/sda2 /mnt/root`.

## The Big Move

There is just one command you need (from the Arch wiki at that);

    sudo rsync -aAXv --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found","/home/"} / /mnt/root

If you're also moving `/home`, then remove the `,"/home/"` from the exclude list. Now, grab a beer and sit back while a list of copied files scrolls on by - and if `/home` is being moved, watch your life flash before your eyes!

/TODO: breakdown of rsync commands

Once everything has been copied over, it's time to fix a few small things before they become problems.

### fstab

`/etc/fstab` is what controls what gets mounted where at boot time. This will normally contain entries for `/boot`, `/`, `/home`, and `swap`, possibly `/boot/efi` too.

Mount the disk that you copied root to, `sudo mount /dev/sdax /mnt/moved`, and using an editor with root permissions such as `sudo vim` in a terminal, edit the `fstab`.

### Quick Vi guide;

* `h`, `j`, `k`, `l` are `left`, `down`, `up`, `right` respectively, or use arrow keys.
* `i` activates `insert` mode.
* `esc` drops you back to `command` mode where the following commands can be run.
* `cw` deletes the word at the cursor and enters insert mode.
* `C` will delete the current line and enter insert mode.
* `dw` will `cut` the word at the cursor.
* `dd` will `cut` the line.
* `p` will paste the `cut` line or word.
* `u` will undo the last change.
* `:q` is quit, and `:q!` is force quit without saving.
* `:wq` to save a file and exit.

There is much more to Vi and ViM than those commands, but they are sufficient for quick editing. Please check out (Vim Tutor)[http://www.openvim.com/] if you want to learn more about one of the most powerful and intuitive editors around.

The fstab  HDD references generally use a UUID for identification, this is a unique identifier that looks similar too . To get this, open another terminal and run `sudo blkid`. This will give you a list of UUID for each partition, one of which you will need for the fstab.

### Bootloader

The last step is to update the bootloader, you may or may not want to boot to the new partition  (and delete the old one) before doing this. If you want to boot first and check all is okay, reboot and then on the grub screen press `c` to get to a console, the next steps are;

* `set root (hd0,2)`, where hd0 is the disk the new partition is on, and the partition number starts at 0.
* `linux /boot/vmlinuz root=/dev/sda2` - this sets the kernel to boot, and the root partition. Generally the kernel is symlinked as *vmlinuz* but this isn't always true.
* `initrd /boot/initrd` sets the ram disk for initial booting. This contains necessary stuff such as kernel modules for booting.

When using the grub console, you can press `tab` to autocomplete commands or get a list. This is also handy when checking which drives and partitions are available, eg `set root (hd` followed by `tab ` will print a list of drives and the partitions on them. `linux /boot/v` then `tab` will print a list of all files starting with 'v' or autocomplete if only one file starts with 'v'.

Once booted, or if you prefer to update the bootloader first, open a terminal and run;

    $> grub-mkconfig -o /boot/grub/grub.cfg

This will output a new config with the new partition included. Reboot and select the new entry for the new partition (you can check this by pressing 'e' on the entry and verifying that the partitions are listed correctly for root/linux).

If the new entry hasn't shown up at all, you may need to check that grub's os-prober hasn't been disabled - run `sudo vi /etc/default/grub` in a terminal look for a line like `GRUB_DISABLE_OS_PROBER="false"`.

### Booted!

So you've booted successfully from the new partition; if you haven't already, run the grub commands above to ensure you can boot from it easily next time. And if you feel like it, delete the old partition.
