+++
title = "What I wish I knew about flutter first"
description = "These are some of the things I wish I knew about flutter before I started developing a large application"
date = 2020-07-27
draft = true
+++

# Flutter

## Architecture

Application state and stuff

## Streams and state

Making sure listeners are closed/cancelled

## Getters and Setters

If you use getters and setters, where either of those has a side effect (for
example putting incoming data in a stream) then something to be mindful of is
that direct manipulation via a getter means the setter does not run.

## Context

The context is different per widget and inherited downwards.

Example: getting the size or position of something in a GestureDetector or Alert

## Timers

Timers should be saved if they are in a state or triggered via a cancellable
function/stream, so they can cancelled when the class whatever is disposed or listener
cancelled.
