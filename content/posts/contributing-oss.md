+++
title = "Contributing to OSS"
description = "Using Polari IRC client as an example"
date = 2019-01-30
+++

I had originally written this post in `date = 2017-03-15` but for some reason never posted it. I'm
trying to clear out a backlog of posts so here it is.

Some questions I've heard over the years, and asked myself are "How do you get in to contributing
to OSS?". I was never really quite sure.

I've been using GNOME for a while now, a long while. The project is a masterpiece of clean and
fresh design, simplicity, and gets out of your way while working. Many of the applications included
in the GNOME ecosystem follow this paradigm.

One of these applications is Polari, an IRC client. It has a nice minimalist design, is functional
and clean yet usable. In short, it does what is needed to get a user up and chatting quickly and
with minimal fuss.

It did have a downside though, as I discovered; long user nicknames in chat could cause issues.
There were even two bug-reports filed regarding this particular issue.

* Some nicknames could cause an extra indent between the nick and message.
* There was no safe-guard against irresponsible users who insisted on using long nicks such as

  `WindowsBunnyIsSuspiciousOfFish`.

What was happening as a result of the above is two things:

* The pixel length of a nick was calculated via the *average* pixel width of a fonts char set, and

  multiplied by number of chars in the nick. As such, if a nickname used chars that put its *actual*

  pixel length over the calculated length, then an extra indent was inserted to compensate.
* The indent between the displayed nick and its associated message is set by the above, so

  `WindowsBunnyIsSuspiciousOfFish` would shunt the indent over that far, leaving a massive void

  between regular nicknames and their associated message.

This issue was enough for me to stop using particular channels where that one user participated.
This of course meant I wasn't using those channels, and that is *not* good enough for anyone.

So I'd found what would be classed as a gronked/needed feature, and set out to fix it. And
contributing was as easy as that; find an issue, or bug-report, fix the problem/feature and submit.
*Note:* nearly one and a half years later (engaged with maintainers before gitlab move), and after
quite aggressively refactoring/trimming code down, the
[MR](https://gitlab.gnome.org/GNOME/polari/merge_requests/29) is still not accepted.

### How easy is it to contribute?

I have to say that GNOME is one of the more friendlier and easier *larger* projects to contribute
to. There are many well written **newbie** guides to peruse such as [this one for Gnome
Builder](https://wiki.gnome.org/Apps/Builder/Contribute) - which rapidly rising to be the chosen
development environment for Gnome applications. And the GNOME [code contribution
guide](https://wiki.gnome.org/Newcomers/CodeContributionWorkflow).

Essentially your workflow (for code contributions) will look like:

1. Set up git
2. Checkout the code repository for your chosen project
3. Choose a bug to work on
4. Create a branch for the work you do (with a name relating to the bug you're working on)
5. Once work is completed, submit a patch for review

It's as simple as that. Really. Many other OSS projects will have a similar workflow to this, and
in the case of projects hosted on github, you will only need to submit a pull request once you
complete the work.

### The Importance of Commit Messages

This is something I think a lot of people get wrong when they first start off - commit messages.

If you look at a git log, on github for example, you will usually see only the first line of a
commit message. For this reason, it is very good practice for you to get in the habit of writing a
short, concise description of the commit that fits a single line - and then expand on that line in
a new paragraph.

Why? Not only will this help others when analysing git logs, but it will help you in the long run
by making it easy to see what was changed, why, and where. One of the best blog posts I've found
that expounded on this is by [Chris Beams](https://chris.beams.io/posts/git-commit/) - make sure to
check it out.

Some projects have a guide for how they prefer their commit messages to be written (normally a
plain text file named `CONTRIBUTING`); if this is the case, be sure to follow it.

Example of GNOME preferred commit message;

    Short title describing your change

    Context of the bug/issue/change.

    Problem that it intends to fix and when happens.

    Fix.

    Bugzilla link

### The First Task

The first thing I wanted to do was shorten the nicknames that went over a certain limit. But before
I could start, I needed to checkout the source, and build it to ensure I could test the work I do.

For Polari, this was made dead easy by [Gnome Builder](https://wiki.gnome.org/Apps/Builder) which
Christian Hergert works on full time. One of the powerful features of Builder, is the ability to
compile and run programs in a [Flatpak](http://flatpak.org/) environment - for GNOME you can use
either stable or nightly Flatpak enviroments (flatpak in itself is an incredible piece of work).

There are other ways to compile GNOME software, such as using jhbuild (which I never got working
due to repo errors), and in the case of openSUSE, using the (Open Build
Service)[https://build.opensuse.org/]. For non-GNOME projects, the project itself will *generally*
include some form of instruction on how to build the software from scratch.

### Other Ways to Contribute to OSS

You don't have to be a programming wiz to contribute to the world of OSS. There are a great deal of
other ways you can help:

* Testing and writing bug reports
* Art and design
* Documentation - general, guides to setting up software or using it)
* Translations - The OSS world has a very far reach, translations are always needed.
* Advocate for the use of OSS
* Organising events around OSS
* Website design and development - Good websites are critical.

And this is just a short list! Generally speaking, if you think a project could benefit from
something you can provide, go ahead and do it! Contact those people responsible for the project via
their chosen means, tell them what you can offer, and go from there.
