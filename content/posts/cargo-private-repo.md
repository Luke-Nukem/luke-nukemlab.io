+++

title = "Private Bitbucket in Pipeline with Cargo"
description = "Ever need to use private repos for deps with Cargo, and need to access them in pipelines?"
date = 2019-01-19
+++

Let's say you have a dependency to a private repo in `Cargo.toml` such as:

``` toml
[dependencies]
sdl2 = { git = "ssh://bitbucket.org/somebody/cool-sdl2-stuff.git" }
```

and you want to run your tests in the Bitbucket pipeline. You'll get an error:


```
    Updating crates.io index
    Updating git repository `ssh://bitbucket.org/somebody/cool-sdl2-stuff.git`
error: failed to load source for a dependency on `cool-sdl2-stuff`
Caused by:
  Unable to update ssh://bitbucket.org/somebody/cool-sdl2-stuff.git#3483733b
Caused by:
  failed to clone into: /home/rust/.cargo/git/db/cool-sdl2-stuff-b9d20094d77d7795
Caused by:
  failed to authenticate when downloading repository
attempted ssh-agent authentication, but none of the usernames `git` succeeded
Caused by:
  error authenticating: no auth sock variable; class=Ssh (23)
```

as it turns out, pipelines does add the access keys to the run, but it isn't using the usual
`~/.ssh/id_rsa` name, so `cargo` can't find the keys.

The solution is to add this to your step script:

```
script:
  - eval `ssh-agent -s`
  - ssh-add ~/.ssh/id_pipelines
  - ssh-add -l
```

And you're good to go.
