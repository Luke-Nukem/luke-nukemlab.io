+++
title = "About Me"
template = "page.html"
+++
* **LinkedIn:**
[https://www.linkedin.com/in/luke-d-jones/](https://www.linkedin.com/in/luke-d-jones/)
* **My Resume**: [download](/Luke_Jones_Software_Engineer-Resume.pdf)

I'm an engineer. At heart, always an engineer. I'm curious about everything, and
will happily dig in to how somehting works. Be it engineering with a lathe, a 60-ton
brake-press, and welder, or with a soldering iron and a handful of magical electronic
components, or writing a game engine, or contributing to open-source projects.

What I really like to do is write programs. Code opens up a unique world of
opportunity to create magic.

I'm also mad about the excellent [Rust](https://www.rust-lang.org/) programming
language, and am busy learning it as fast as I can; this isn't the easiest thing
to do and requires that you understand computers at a much lower level than many
people are accustomed to.

## A Short Work History

I spent over a decade working as an engineer specialising
in heavy structural fabrication. Welding and fabrication, with a dose of machining.
Often I'd be given a spec of some sort, and would have to design and then fabricate
the complete structure using both CAD and manual drawing. I bounced between various
industries for a few years, even working with one of New Zealands top dirt-track racers
to build stockcar and super-stock chassis - I even got 3 seconds of fame being featured
in a documentary about the speedway scene here.

I now work at Sphere Identity where I've focused largely on learning encryption and
the many complexities behind, along with designing a protocol to be used for sharing
encrypted data. Written in Rust of course ;)

## Interests

Dedicated Linux user and abuser. I've been using Linux since my teenaged years
where I first ran Slackware on an AMD K6/500mhz; it's been a heck of a ride.

Gaming? Not so much these days. Though I am rather enjoying the craft of *making*
games. At a pinch I'll always pull up some classic Doom, or Quake for instant
stress relief.

Arcade machines. I ran GarageArcades for a little while as a hobby. This was a
small business of sorts where I built custom arcade machines for people. It was
a very enjoyable experience to see people amazed at the quality I produced.

