+++
title = "My Projects"
template = "page.html"
+++

This page is a selection of personal projects I have worked on or actively work on.

<h2>Asus Linux support</h2>

Writing drivers, kernel patches, and tools to support ASUS laptops in Linux.

[[website]](https://asus-linux.org/)
[[Source]](https://gitlab.com/asus-linux/asusctl)

<h2>room4doom</h2>

**Rewriting Doom engine in Rust**

[[Docs]](https://flukejones.gitlab.io/room4doom)
[[Source]](https://gitlab.com/flukejones/room4doom)

<h2>cyclone-rs</h2>

A physics engine work in progress. Written by following the "Game Physics Engine Development" book by Ian Millington.

Currently has a 2d engine which works quite well.

[[Docs]](https://flukejones.gitlab.io/cyclone-rs/particle2d/)
[[Source]](https://gitlab.com/flukejones/cyclone-rs)

<h2>GSoC 2017 Project Summary</h2>

The goal of this project was to research the use of the Rust programming language in GNOME [GJS](https://wiki.gnome.org/Projects/Gjs) to help reduce or eliminate memory leaks and increase memory safety, and where possible, implement it to do so.

[[Link]](/gsoc-2017/)

<h2>A Game Experiment</h2>

Using Rust and SDL2.

This is a mamoth project, and has involved a lot of lessons for me. Essentially
it is to become a kind of minimal framework for creation of a 2D game - the basics
are there and working, but it requires much more work and refinement.

Recently I broke out various parts of the engine in to their own crates, some of which are published on [crates.io](https://crates.io).

[[Docs]](https://flukejones.gitlab.io/rust-game/game/)
[[Source]](http://gitlab.com/flukejones/rust-game)

<h2>Published Crates</h2>
<h3>tiny_ecs</h3>

The intention of this crate is that a basic ECS is provided, where you will be required to exercise a little additional control. This is somewhat due to some limitations, and also due to trying to maintain as little overhead as possible - this means no unneccesary copies/clones.

Where most other ECS crates provide a mechanism for inserting "systems" in to the ECS to run against entities, this one leaves it out - you can think of it as a "system for entity/components". You will need to create external systems; these can be a function, a loop, or anything else.

[[crates.io]](https://crates.io/crates/tiny_ecs)
[[Docs]](https://docs.rs/tiny_ecs/)
[[Source]](https://gitlab.com/flukejones/tiny_ecs)

<h3>tiled-json-rs</h3>

A handy crate for parsing the Tiled JSON data in to a usable structure.

The crate includes a few small helper functions on Map, TileSet, and TileLayer. These functions are for common tasks such as generating a cloumn/row location (tiles are stored in a 1D array), a located box on an image for helping with tile-to-tilesheet image picking, and loading files (or strings).

[[crates.io]](https://crates.io/crates/tiled-json-rs)
[[Docs]](https://docs.rs/tiled-json-rs/)
[[Source]](https://gitlab.com/flukejones/tiled-json-rs)

<h2>Game of Life - Written in Rust</h2>

**This code is rather old now**

This is a project I'm working on bit by bit. It was an excellent way to learn
some of the more nuanced aspects of **Rust** and resulted in some rather messy
code at times. I also experimented quite heavily with it, from using straight
nested `Vec` to using `maps` throughout.

![animated](./gifs/animated.gif "Life from a tetromino")

The code has gone through some pretty hefty evolutions at times, and now has many
features on the way.

[[Docs]](https://flukejones.gitlab.io/rs_life/life/)
[[Source]](http://gitlab.com/flukejones/rs_life)
